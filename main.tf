data "aws_iam_policy" "ebs_csi_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
}

resource "random_string" "suffix" {
  length  = 12
  special = false
}

locals {
  kube_cluster_name = (var.eks_cluster_name != "changeme" ? var.eks_cluster_name : "kubernetes-${random_string.suffix.result}")
}


module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_name = local.kube_cluster_name
  cluster_version = var.eks_kube_version

  cluster_endpoint_public_access = var.eks_cluster_endpoint_public_access
  enable_cluster_creator_admin_permissions = var.eks_cluster_creator_admin_perms

  cluster_addons = {
    aws-ebs-csi-driver = {
      service_account_role_arn = module.irsa-ebs-csi.iam_role_arn
    }
  }

  vpc_id = var.vpc_id
  subnet_ids = var.private_subnets

  eks_managed_node_group_defaults = {
    ami_type = var.eks_node_group_ami_type_default 
  }

  eks_managed_node_groups = {
    worker_group_1 = {
      name = var.eks_node_group_name 
      instance_types = var.eks_instance_type_list 
      min_size = var.eks_min_size
      max_size = var.eks_max_size
      desired_size = var.eks_desired_size
    }
  }
}

#IAM Role for Service Account (EBS-CSI)
module "irsa-ebs-csi" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"

  create_role                   = true
  role_name                     = "AmazonEKSTFEBSCSIRole-${module.eks.cluster_name}"
  provider_url                  = module.eks.oidc_provider
  role_policy_arns              = [data.aws_iam_policy.ebs_csi_policy.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:ebs-csi-controller-sa"]
}

