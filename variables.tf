#===========================
# REQUIRED
#===========================
variable "vpc_id" {
  type        = string
  description = "VPC ID to deploy cluster to"
}
variable "private_subnets" {
  type = list
  description = "list of private subnets (1.2.3.4/24) to deploy to"
}
#===========================
# OPTIONAL
#===========================
variable "eks_cluster_name" {
  type        = string
  description = "the name of the cluster, default: kubernetes-{random string}"
  default     = "changeme" #used as a conditional value, don't change this
}
variable "eks_kube_version" {
  type        = string
  description = "the version of kubernetes to deploy, default: 1.28"
  default     = "1.28"
}
variable "eks_cluster_endpoint_public_access" {
  type        = bool
  description = "whether to configure public access to the cluster endpoint, default: true"
  default     = true
}
variable "eks_cluster_creator_admin_perms" {
  type        = bool
  description = "whether to grant the cluster creator admin access, default: true"
  default     = true
}
variable "eks_node_group_ami_type_default" {
  type        = string
  description = "AMI type default for node group, default: AL2_x86_64"
  default     = "AL2_x86_64"
}
variable "eks_node_group_name" {
  type        = string
  description = "name of the node group, default: workers"
  default     = "workers"
}
variable "eks_instance_type_list" {
  type        = list
  description = "list of instance types for node group"
  default     = ["t3.small"] 
} 
variable "eks_min_size" {
  type        = number
  description = "the minimum number of kube nodes in the node group"
  default     = 1
}
variable "eks_max_size" {
  type        = number
  description = "the maximum number of kube nodes in the node group"
  default     = 3
}
variable "eks_desired_size" {
  type        = number
  description = "the desired number of kube nodes in a node group"
  default     = 3
}
