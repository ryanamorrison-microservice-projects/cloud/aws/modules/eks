output "eks_cluster_endpoint" {
  value       = module.eks.cluster_endpoint
  description = "Kubernetes control plane endpoint (API) address"
}
output "eks_cluster_security_group_id" {
  value       = module.eks.cluster_security_group_id
  description = "security group ID's attached to the control plane"
}
output "eks_cluster_name" {
  value       = module.eks.cluster_name
  description = "Kubernetes cluster name"
}
